# Generated by Django 2.0.1 on 2018-01-20 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='symbols',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
    ]
